#include <map>
#include "TMath.h"
#include "TF1.h"
#include "TH1D.h"
#include "TFile.h"

#define MAX_NJETS_LJETS 10
#define MAX_NJETS_DIL 8


// customise to your own path
// TString TRExFitter_path = "/pc2014-data3/yaqin/4tops/TRExFitter/4tops20190409/4tops_1los_commonconfig/results/test_derive_reweighting_njets/Histograms/Histograms.root";
// TString TRExFitter_path = "./results/test_variable_validation_derive_reweighting_nJets_HT_all_red/Histograms/Histograms.root";
TString TRExFitter_path = "Histograms.root";

const char* histo_form_reweighting = "%s_%sj2b_VR__%s"; // <channel>_<N>j2b_VR__<var_name>
const char* histo_form_nominal     = "%s_%sj2b_VR__%s_ttbar"; // <channel>_<N>j2b_VR__<var_name>_ttbar
const char* histo_form_syst        = "%s_%sj2b_VR__%s_%s";    // <channel>_<N>j2b_VR__<var_name>_<ttbar_syst>

void setup_reweighting_function(TF1* func, int nlep, int nJets, TString var) {

  // run ./fit_rw_function.py to obtain the following file - alternatively, set the parameters manually
  if(var == "HT_all_red") {
#include "reweighting_functions_HT_all_red.C"
  }

// if( nJets == 5 ) func->SetParameters( 0.517765963001, 36.005645792, 0.675633130213 );
// if( nJets == 6 ) func->SetParameters( 0.122466304107, 19.0097245798, 0.463585048233 );
// if( nJets == 7 ) func->SetParameters( -14.0128999364, 20.0762270263, 0.04243505473 );
// if( nJets >= 8 ) func->SetParameters( -13.0438129129, 20.1455454933, 0.0493544460577 );

};


// do not touch anything below

std::map<std::pair<int, int>, std::string> syst_map = {
  // ttbar modelling uncert
  {{1, 1}, "ttbar_aMcAtNloPy8"},
  {{1, 2}, "ttbar_TTL_aMcAtNloPy8"},
  {{1, 3}, "ttbar_TTC_aMcAtNloPy8"},
  {{1, 4}, "ttbar_TTB_aMcAtNloPy8"},
  {{1, 5}, "ttbar_PhHerwig"},
  {{1, 6}, "ttbar_TTL_PhHerwig"},
  {{1, 7}, "ttbar_TTC_PhHerwig"},
  {{1, 8}, "ttbar_TTB_PhHerwig"},
  {{1, 9}, "ttbar_PhPy8_Rad"},
  {{1, 10}, "ttbar_TTL_PhPy8_Rad"},
  {{1, 11}, "ttbar_TTC_PhPy8_Rad"},
  {{1, 12}, "ttbar_TTB_PhPy8_Rad"},

  // JES
  {{2, 0}, "ttbar_ATLAS_JES_BJES_Response_Up"},
  {{2, 1}, "ttbar_ATLAS_JES_BJES_Response_Down"},
  {{2, 2}, "ttbar_ATLAS_JES_EffectiveNP_Detector1_Up"},
  {{2, 3}, "ttbar_ATLAS_JES_EffectiveNP_Detector1_Down"},
  {{2, 4}, "ttbar_ATLAS_JES_EffectiveNP_Detector2_Up"},
  {{2, 5}, "ttbar_ATLAS_JES_EffectiveNP_Detector2_Down"},
  {{2, 6}, "ttbar_ATLAS_JES_EffectiveNP_Mixed1_Up"},
  {{2, 7}, "ttbar_ATLAS_JES_EffectiveNP_Mixed1_Down"},
  {{2, 8}, "ttbar_ATLAS_JES_EffectiveNP_Mixed2_Up"},
  {{2, 9}, "ttbar_ATLAS_JES_EffectiveNP_Mixed2_Down"},
  {{2, 10}, "ttbar_ATLAS_JES_EffectiveNP_Mixed3_Up"},
  {{2, 11}, "ttbar_ATLAS_JES_EffectiveNP_Mixed3_Down"},
  {{2, 12}, "ttbar_ATLAS_JES_EffectiveNP_Modelling1_Up"},
  {{2, 13}, "ttbar_ATLAS_JES_EffectiveNP_Modelling1_Down"},
  {{2, 14}, "ttbar_ATLAS_JES_EffectiveNP_Modelling2_Up"},
  {{2, 15}, "ttbar_ATLAS_JES_EffectiveNP_Modelling2_Down"},
  {{2, 16}, "ttbar_ATLAS_JES_EffectiveNP_Modelling3_Up"},
  {{2, 17}, "ttbar_ATLAS_JES_EffectiveNP_Modelling3_Down"},
  {{2, 18}, "ttbar_ATLAS_JES_EffectiveNP_Modelling4_Up"},
  {{2, 19}, "ttbar_ATLAS_JES_EffectiveNP_Modelling4_Down"},
  {{2, 20}, "ttbar_ATLAS_JES_EffectiveNP_Statistical1_Up"},
  {{2, 21}, "ttbar_ATLAS_JES_EffectiveNP_Statistical1_Down"},
  {{2, 22}, "ttbar_ATLAS_JES_EffectiveNP_Statistical2_Up"},
  {{2, 23}, "ttbar_ATLAS_JES_EffectiveNP_Statistical2_Down"},
  {{2, 24}, "ttbar_ATLAS_JES_EffectiveNP_Statistical3_Up"},
  {{2, 25}, "ttbar_ATLAS_JES_EffectiveNP_Statistical3_Down"},
  {{2, 26}, "ttbar_ATLAS_JES_EffectiveNP_Statistical4_Up"},
  {{2, 27}, "ttbar_ATLAS_JES_EffectiveNP_Statistical4_Down"},
  {{2, 28}, "ttbar_ATLAS_JES_EffectiveNP_Statistical5_Up"},
  {{2, 29}, "ttbar_ATLAS_JES_EffectiveNP_Statistical5_Down"},
  {{2, 30}, "ttbar_ATLAS_JES_EffectiveNP_Statistical6_Up"},
  {{2, 31}, "ttbar_ATLAS_JES_EffectiveNP_Statistical6_Down"},
  {{2, 32}, "ttbar_ATLAS_JES_EtaIntercalibration_Modelling_Up"},
  {{2, 33}, "ttbar_ATLAS_JES_EtaIntercalibration_Modelling_Down"},
  {{2, 34}, "ttbar_ATLAS_JES_EtaIntercalibration_NonClosure_highE_Up"},
  {{2, 35}, "ttbar_ATLAS_JES_EtaIntercalibration_NonClosure_highE_Down"},
  {{2, 36}, "ttbar_ATLAS_JES_EtaIntercalibration_NonClosure_negEta_Up"},
  {{2, 37}, "ttbar_ATLAS_JES_EtaIntercalibration_NonClosure_negEta_Down"},
  {{2, 38}, "ttbar_ATLAS_JES_EtaIntercalibration_NonClosure_posEta_Up"},
  {{2, 39}, "ttbar_ATLAS_JES_EtaIntercalibration_NonClosure_posEta_Down"},
  {{2, 40}, "ttbar_ATLAS_JES_EtaIntercalibration_TotalStat_Up"},
  {{2, 41}, "ttbar_ATLAS_JES_EtaIntercalibration_TotalStat_Down"},
  {{2, 42}, "ttbar_ATLAS_JES_Flavor_Composition_Up"},
  {{2, 43}, "ttbar_ATLAS_JES_Flavor_Composition_Down"},
  {{2, 44}, "ttbar_ATLAS_JES_Flavor_Response_Up"},
  {{2, 45}, "ttbar_ATLAS_JES_Flavor_Response_Down"},
  {{2, 46}, "ttbar_ATLAS_JES_Pileup_OffsetMu_Up"},
  {{2, 47}, "ttbar_ATLAS_JES_Pileup_OffsetMu_Down"},
  {{2, 48}, "ttbar_ATLAS_JES_Pileup_OffsetNPV_Up"},
  {{2, 49}, "ttbar_ATLAS_JES_Pileup_OffsetNPV_Down"},
  {{2, 50}, "ttbar_ATLAS_JES_Pileup_PtTerm_Up"},
  {{2, 51}, "ttbar_ATLAS_JES_Pileup_PtTerm_Down"},
  {{2, 52}, "ttbar_ATLAS_JES_Pileup_RhoTopology_Up"},
  {{2, 53}, "ttbar_ATLAS_JES_Pileup_RhoTopology_Down"},
  {{2, 54}, "ttbar_ATLAS_JES_PunchThrough_MC16_Up"},
  {{2, 55}, "ttbar_ATLAS_JES_PunchThrough_MC16_Down"},
  {{2, 56}, "ttbar_ATLAS_JES_SingleParticle_HighPt_Up"},
  {{2, 57}, "ttbar_ATLAS_JES_SingleParticle_HighPt_Down"},
  {{2, 58}, "ttbar_ATLAS_JER_DataVsMC_Up"},
  {{2, 59}, "ttbar_ATLAS_JER_DataVsMC_Down"},
  {{2, 60}, "ttbar_ATLAS_JER_EffectiveNP_1_Up"},
  {{2, 61}, "ttbar_ATLAS_JER_EffectiveNP_1_Down"},
  {{2, 62}, "ttbar_ATLAS_JER_EffectiveNP_2_Up"},
  {{2, 63}, "ttbar_ATLAS_JER_EffectiveNP_2_Down"},
  {{2, 64}, "ttbar_ATLAS_JER_EffectiveNP_3_Up"},
  {{2, 65}, "ttbar_ATLAS_JER_EffectiveNP_3_Down"},
  {{2, 66}, "ttbar_ATLAS_JER_EffectiveNP_4_Up"},
  {{2, 67}, "ttbar_ATLAS_JER_EffectiveNP_4_Down"},
  {{2, 68}, "ttbar_ATLAS_JER_EffectiveNP_5_Up"},
  {{2, 69}, "ttbar_ATLAS_JER_EffectiveNP_5_Down"},
  {{2, 70}, "ttbar_ATLAS_JER_EffectiveNP_6_Up"},
  {{2, 71}, "ttbar_ATLAS_JER_EffectiveNP_6_Down"},
  {{2, 72}, "ttbar_ATLAS_JER_EffectiveNP_7restTerm_Up"},
  {{2, 73}, "ttbar_ATLAS_JER_EffectiveNP_7restTerm_Down"},

  // FTAG_B
  {{10, 0}, "ttbar_ATLAS_FTAG_MV2c10_B0_Up"},
  {{10, 1}, "ttbar_ATLAS_FTAG_MV2c10_B0_Down"},
  {{10, 2}, "ttbar_ATLAS_FTAG_MV2c10_B1_Up"},
  {{10, 3}, "ttbar_ATLAS_FTAG_MV2c10_B1_Down"},
  {{10, 4}, "ttbar_ATLAS_FTAG_MV2c10_B2_Up"},
  {{10, 5}, "ttbar_ATLAS_FTAG_MV2c10_B2_Down"},
  {{10, 6}, "ttbar_ATLAS_FTAG_MV2c10_B3_Up"},
  {{10, 7}, "ttbar_ATLAS_FTAG_MV2c10_B3_Down"},
  {{10, 8}, "ttbar_ATLAS_FTAG_MV2c10_B4_Up"},
  {{10, 9}, "ttbar_ATLAS_FTAG_MV2c10_B4_Down"},
  {{10, 10}, "ttbar_ATLAS_FTAG_MV2c10_B5_Up"},
  {{10, 11}, "ttbar_ATLAS_FTAG_MV2c10_B5_Down"},
  {{10, 12}, "ttbar_ATLAS_FTAG_MV2c10_B6_Up"},
  {{10, 13}, "ttbar_ATLAS_FTAG_MV2c10_B6_Down"},
  {{10, 14}, "ttbar_ATLAS_FTAG_MV2c10_B7_Up"},
  {{10, 15}, "ttbar_ATLAS_FTAG_MV2c10_B7_Down"},
  {{10, 16}, "ttbar_ATLAS_FTAG_MV2c10_B8_Up"},
  {{10, 17}, "ttbar_ATLAS_FTAG_MV2c10_B8_Down"},
  {{10, 18}, "ttbar_ATLAS_FTAG_MV2c10_B9_Up"},
  {{10, 19}, "ttbar_ATLAS_FTAG_MV2c10_B9_Down"},
  {{10, 20}, "ttbar_ATLAS_FTAG_MV2c10_B10_Up"},
  {{10, 21}, "ttbar_ATLAS_FTAG_MV2c10_B10_Down"},
  {{10, 22}, "ttbar_ATLAS_FTAG_MV2c10_B11_Up"},
  {{10, 23}, "ttbar_ATLAS_FTAG_MV2c10_B11_Down"},
  {{10, 24}, "ttbar_ATLAS_FTAG_MV2c10_B12_Up"},
  {{10, 25}, "ttbar_ATLAS_FTAG_MV2c10_B12_Down"},
  {{10, 26}, "ttbar_ATLAS_FTAG_MV2c10_B13_Up"},
  {{10, 27}, "ttbar_ATLAS_FTAG_MV2c10_B13_Down"},
  {{10, 28}, "ttbar_ATLAS_FTAG_MV2c10_B14_Up"},
  {{10, 29}, "ttbar_ATLAS_FTAG_MV2c10_B14_Down"},
  {{10, 30}, "ttbar_ATLAS_FTAG_MV2c10_B15_Up"},
  {{10, 31}, "ttbar_ATLAS_FTAG_MV2c10_B15_Down"},
  {{10, 32}, "ttbar_ATLAS_FTAG_MV2c10_B16_Up"},
  {{10, 33}, "ttbar_ATLAS_FTAG_MV2c10_B16_Down"},
  {{10, 34}, "ttbar_ATLAS_FTAG_MV2c10_B17_Up"},
  {{10, 35}, "ttbar_ATLAS_FTAG_MV2c10_B17_Down"},
  {{10, 36}, "ttbar_ATLAS_FTAG_MV2c10_B18_Up"},
  {{10, 37}, "ttbar_ATLAS_FTAG_MV2c10_B18_Down"},
  {{10, 38}, "ttbar_ATLAS_FTAG_MV2c10_B19_Up"},
  {{10, 39}, "ttbar_ATLAS_FTAG_MV2c10_B19_Down"},
  {{10, 40}, "ttbar_ATLAS_FTAG_MV2c10_B20_Up"},
  {{10, 41}, "ttbar_ATLAS_FTAG_MV2c10_B20_Down"},
  {{10, 42}, "ttbar_ATLAS_FTAG_MV2c10_B21_Up"},
  {{10, 43}, "ttbar_ATLAS_FTAG_MV2c10_B21_Down"},
  {{10, 44}, "ttbar_ATLAS_FTAG_MV2c10_B22_Up"},
  {{10, 45}, "ttbar_ATLAS_FTAG_MV2c10_B22_Down"},
  {{10, 46}, "ttbar_ATLAS_FTAG_MV2c10_B23_Up"},
  {{10, 47}, "ttbar_ATLAS_FTAG_MV2c10_B23_Down"},
  {{10, 48}, "ttbar_ATLAS_FTAG_MV2c10_B24_Up"},
  {{10, 49}, "ttbar_ATLAS_FTAG_MV2c10_B24_Down"},
  {{10, 50}, "ttbar_ATLAS_FTAG_MV2c10_B25_Up"},
  {{10, 51}, "ttbar_ATLAS_FTAG_MV2c10_B25_Down"},
  {{10, 52}, "ttbar_ATLAS_FTAG_MV2c10_B26_Up"},
  {{10, 53}, "ttbar_ATLAS_FTAG_MV2c10_B26_Down"},
  {{10, 54}, "ttbar_ATLAS_FTAG_MV2c10_B27_Up"},
  {{10, 55}, "ttbar_ATLAS_FTAG_MV2c10_B27_Down"},
  {{10, 56}, "ttbar_ATLAS_FTAG_MV2c10_B28_Up"},
  {{10, 57}, "ttbar_ATLAS_FTAG_MV2c10_B28_Down"},
  {{10, 58}, "ttbar_ATLAS_FTAG_MV2c10_B29_Up"},
  {{10, 59}, "ttbar_ATLAS_FTAG_MV2c10_B29_Down"},
  {{10, 60}, "ttbar_ATLAS_FTAG_MV2c10_B30_Up"},
  {{10, 61}, "ttbar_ATLAS_FTAG_MV2c10_B30_Down"},
  {{10, 62}, "ttbar_ATLAS_FTAG_MV2c10_B31_Up"},
  {{10, 63}, "ttbar_ATLAS_FTAG_MV2c10_B31_Down"},
  {{10, 64}, "ttbar_ATLAS_FTAG_MV2c10_B32_Up"},
  {{10, 65}, "ttbar_ATLAS_FTAG_MV2c10_B32_Down"},
  {{10, 66}, "ttbar_ATLAS_FTAG_MV2c10_B33_Up"},
  {{10, 67}, "ttbar_ATLAS_FTAG_MV2c10_B33_Down"},
  {{10, 68}, "ttbar_ATLAS_FTAG_MV2c10_B34_Up"},
  {{10, 69}, "ttbar_ATLAS_FTAG_MV2c10_B34_Down"},
  {{10, 70}, "ttbar_ATLAS_FTAG_MV2c10_B35_Up"},
  {{10, 71}, "ttbar_ATLAS_FTAG_MV2c10_B35_Down"},
  {{10, 72}, "ttbar_ATLAS_FTAG_MV2c10_B36_Up"},
  {{10, 73}, "ttbar_ATLAS_FTAG_MV2c10_B36_Down"},
  {{10, 74}, "ttbar_ATLAS_FTAG_MV2c10_B37_Up"},
  {{10, 75}, "ttbar_ATLAS_FTAG_MV2c10_B37_Down"},
  {{10, 76}, "ttbar_ATLAS_FTAG_MV2c10_B38_Up"},
  {{10, 77}, "ttbar_ATLAS_FTAG_MV2c10_B38_Down"},
  {{10, 78}, "ttbar_ATLAS_FTAG_MV2c10_B39_Up"},
  {{10, 79}, "ttbar_ATLAS_FTAG_MV2c10_B39_Down"},
  {{10, 80}, "ttbar_ATLAS_FTAG_MV2c10_B40_Up"},
  {{10, 81}, "ttbar_ATLAS_FTAG_MV2c10_B40_Down"},
  {{10, 82}, "ttbar_ATLAS_FTAG_MV2c10_B41_Up"},
  {{10, 83}, "ttbar_ATLAS_FTAG_MV2c10_B41_Down"},
  {{10, 84}, "ttbar_ATLAS_FTAG_MV2c10_B42_Up"},
  {{10, 85}, "ttbar_ATLAS_FTAG_MV2c10_B42_Down"},
  {{10, 86}, "ttbar_ATLAS_FTAG_MV2c10_B43_Up"},
  {{10, 87}, "ttbar_ATLAS_FTAG_MV2c10_B43_Down"},
  {{10, 88}, "ttbar_ATLAS_FTAG_MV2c10_B44_Up"},
  {{10, 89}, "ttbar_ATLAS_FTAG_MV2c10_B44_Down"},

  // FTAG_C
  {{11, 0}, "ttbar_ATLAS_FTAG_MV2c10_C0_Up"},
  {{11, 1}, "ttbar_ATLAS_FTAG_MV2c10_C0_Down"},
  {{11, 2}, "ttbar_ATLAS_FTAG_MV2c10_C1_Up"},
  {{11, 3}, "ttbar_ATLAS_FTAG_MV2c10_C1_Down"},
  {{11, 4}, "ttbar_ATLAS_FTAG_MV2c10_C2_Up"},
  {{11, 5}, "ttbar_ATLAS_FTAG_MV2c10_C2_Down"},
  {{11, 6}, "ttbar_ATLAS_FTAG_MV2c10_C3_Up"},
  {{11, 7}, "ttbar_ATLAS_FTAG_MV2c10_C3_Down"},
  {{11, 8}, "ttbar_ATLAS_FTAG_MV2c10_C4_Up"},
  {{11, 9}, "ttbar_ATLAS_FTAG_MV2c10_C4_Down"},
  {{11, 10}, "ttbar_ATLAS_FTAG_MV2c10_C5_Up"},
  {{11, 11}, "ttbar_ATLAS_FTAG_MV2c10_C5_Down"},
  {{11, 12}, "ttbar_ATLAS_FTAG_MV2c10_C6_Up"},
  {{11, 13}, "ttbar_ATLAS_FTAG_MV2c10_C6_Down"},
  {{11, 14}, "ttbar_ATLAS_FTAG_MV2c10_C7_Up"},
  {{11, 15}, "ttbar_ATLAS_FTAG_MV2c10_C7_Down"},
  {{11, 16}, "ttbar_ATLAS_FTAG_MV2c10_C8_Up"},
  {{11, 17}, "ttbar_ATLAS_FTAG_MV2c10_C8_Down"},
  {{11, 18}, "ttbar_ATLAS_FTAG_MV2c10_C9_Up"},
  {{11, 19}, "ttbar_ATLAS_FTAG_MV2c10_C9_Down"},
  {{11, 20}, "ttbar_ATLAS_FTAG_MV2c10_C10_Up"},
  {{11, 21}, "ttbar_ATLAS_FTAG_MV2c10_C10_Down"},
  {{11, 22}, "ttbar_ATLAS_FTAG_MV2c10_C11_Up"},
  {{11, 23}, "ttbar_ATLAS_FTAG_MV2c10_C11_Down"},
  {{11, 24}, "ttbar_ATLAS_FTAG_MV2c10_C12_Up"},
  {{11, 25}, "ttbar_ATLAS_FTAG_MV2c10_C12_Down"},
  {{11, 26}, "ttbar_ATLAS_FTAG_MV2c10_C13_Up"},
  {{11, 27}, "ttbar_ATLAS_FTAG_MV2c10_C13_Down"},
  {{11, 28}, "ttbar_ATLAS_FTAG_MV2c10_C14_Up"},
  {{11, 29}, "ttbar_ATLAS_FTAG_MV2c10_C14_Down"},
  {{11, 30}, "ttbar_ATLAS_FTAG_MV2c10_C15_Up"},
  {{11, 31}, "ttbar_ATLAS_FTAG_MV2c10_C15_Down"},
  {{11, 32}, "ttbar_ATLAS_FTAG_MV2c10_C16_Up"},
  {{11, 33}, "ttbar_ATLAS_FTAG_MV2c10_C16_Down"},
  {{11, 34}, "ttbar_ATLAS_FTAG_MV2c10_C17_Up"},
  {{11, 35}, "ttbar_ATLAS_FTAG_MV2c10_C17_Down"},
  {{11, 36}, "ttbar_ATLAS_FTAG_MV2c10_C18_Up"},
  {{11, 37}, "ttbar_ATLAS_FTAG_MV2c10_C18_Down"},
  {{11, 38}, "ttbar_ATLAS_FTAG_MV2c10_C19_Up"},
  {{11, 39}, "ttbar_ATLAS_FTAG_MV2c10_C19_Down"},

  // FTAG_Light
  {{12, 0}, "ttbar_ATLAS_FTAG_MV2c10_Light0_Up"},
  {{12, 1}, "ttbar_ATLAS_FTAG_MV2c10_Light0_Down"},
  {{12, 2}, "ttbar_ATLAS_FTAG_MV2c10_Light1_Up"},
  {{12, 3}, "ttbar_ATLAS_FTAG_MV2c10_Light1_Down"},
  {{12, 4}, "ttbar_ATLAS_FTAG_MV2c10_Light2_Up"},
  {{12, 5}, "ttbar_ATLAS_FTAG_MV2c10_Light2_Down"},
  {{12, 6}, "ttbar_ATLAS_FTAG_MV2c10_Light3_Up"},
  {{12, 7}, "ttbar_ATLAS_FTAG_MV2c10_Light3_Down"},
  {{12, 8}, "ttbar_ATLAS_FTAG_MV2c10_Light4_Up"},
  {{12, 9}, "ttbar_ATLAS_FTAG_MV2c10_Light4_Down"},
  {{12, 10}, "ttbar_ATLAS_FTAG_MV2c10_Light5_Up"},
  {{12, 11}, "ttbar_ATLAS_FTAG_MV2c10_Light5_Down"},
  {{12, 12}, "ttbar_ATLAS_FTAG_MV2c10_Light6_Up"},
  {{12, 13}, "ttbar_ATLAS_FTAG_MV2c10_Light6_Down"},
  {{12, 14}, "ttbar_ATLAS_FTAG_MV2c10_Light7_Up"},
  {{12, 15}, "ttbar_ATLAS_FTAG_MV2c10_Light7_Down"},
  {{12, 16}, "ttbar_ATLAS_FTAG_MV2c10_Light8_Up"},
  {{12, 17}, "ttbar_ATLAS_FTAG_MV2c10_Light8_Down"},
  {{12, 18}, "ttbar_ATLAS_FTAG_MV2c10_Light9_Up"},
  {{12, 19}, "ttbar_ATLAS_FTAG_MV2c10_Light9_Down"},
  {{12, 20}, "ttbar_ATLAS_FTAG_MV2c10_Light10_Up"},
  {{12, 21}, "ttbar_ATLAS_FTAG_MV2c10_Light10_Down"},
  {{12, 22}, "ttbar_ATLAS_FTAG_MV2c10_Light11_Up"},
  {{12, 23}, "ttbar_ATLAS_FTAG_MV2c10_Light11_Down"},
  {{12, 24}, "ttbar_ATLAS_FTAG_MV2c10_Light12_Up"},
  {{12, 25}, "ttbar_ATLAS_FTAG_MV2c10_Light12_Down"},
  {{12, 26}, "ttbar_ATLAS_FTAG_MV2c10_Light13_Up"},
  {{12, 27}, "ttbar_ATLAS_FTAG_MV2c10_Light13_Down"},
  {{12, 28}, "ttbar_ATLAS_FTAG_MV2c10_Light14_Up"},
  {{12, 29}, "ttbar_ATLAS_FTAG_MV2c10_Light14_Down"},
  {{12, 30}, "ttbar_ATLAS_FTAG_MV2c10_Light15_Up"},
  {{12, 31}, "ttbar_ATLAS_FTAG_MV2c10_Light15_Down"},
  {{12, 32}, "ttbar_ATLAS_FTAG_MV2c10_Light16_Up"},
  {{12, 33}, "ttbar_ATLAS_FTAG_MV2c10_Light16_Down"},
  {{12, 34}, "ttbar_ATLAS_FTAG_MV2c10_Light17_Up"},
  {{12, 35}, "ttbar_ATLAS_FTAG_MV2c10_Light17_Down"},
  {{12, 36}, "ttbar_ATLAS_FTAG_MV2c10_Light18_Up"},
  {{12, 37}, "ttbar_ATLAS_FTAG_MV2c10_Light18_Down"},
  {{12, 38}, "ttbar_ATLAS_FTAG_MV2c10_Light19_Up"},
  {{12, 39}, "ttbar_ATLAS_FTAG_MV2c10_Light19_Down"},

  // {{13, 0}, "ttbar_ATLAS_FTAG_MV2c10_extrapolation"},
  // {{13, 1}, "ttbar_ATLAS_FTAG_MV2c10_extrapolation"},
};

TF1 *func_HT_all = 0;
TF1 *func_HT_all_red = 0;
TFile *f_histograms;

int binx = 0;
Double_t d_ratio = 0.;
Double_t value = 0.;

TH1D *h_nJets_reweighting = 0;
TH1D *h_nJets_nom = 0;
TH1D *h_nJets_sys = 0;

TH1D *h_HT_all_nom = 0;
TH1D *h_HT_all_sys = 0;

TH1D *h_HT_all_red_nom = 0;
TH1D *h_HT_all_red_sys = 0;

TH1D *h_deltaR_avg_jets_reweighting = 0;
TH1D *h_deltaR_avg_jets_nom = 0;
TH1D *h_deltaR_avg_jets_sys = 0;


Double_t GetBinContent(TH1D* h, const Double_t var) {  
  binx = h->FindBin(var);
  if(binx < 1) binx = 1;
  if(binx > h->GetNbinsX()) binx = h->GetNbinsX();
  return h->GetBinContent(binx);
}

void GetHistFromFile(TH1D* h, const char* histo_form, const TString chan_name, const TString nj_str, const TString reweighting_var) {
  TString name = TString::Format(histo_form, chan_name.Data(), nj_str.Data(), reweighting_var.Data());
  if(!h || h->GetName() != name) h = (TH1D*)f_histograms->Get(name);
  if( h == 0 ) cerr << "Error::Could not find histogram " << name << endl;
  return ;
}

Double_t MyWeight_nJets(int dummy = 5, int Njets = 5, int nlep = 0, int rat1 = 0, int rat2 = 0) {
  if(!f_histograms){
    f_histograms = new TFile(TRExFitter_path,"READ");
  }

  TString chan_name = "";
  TString reweighting_var = "nJets";
  TString nj_str = "";
  if( nlep == 1 ) {
    chan_name = "ljets";
    nj_str = "ge7";
  }
  else if (nlep == 2) {
    chan_name = "os2l";
    nj_str = "ge5";
  }
  else {
    cerr << "Error::MyWeight()::wrong nlep = " << nlep << endl;
    exit(1);
  }
  TString nJets_reweighting_name = TString::Format(histo_form_reweighting, chan_name.Data(), nj_str.Data(), reweighting_var.Data());
  if(!h_nJets_reweighting || h_nJets_reweighting->GetName() != nJets_reweighting_name) h_nJets_reweighting = (TH1D*)f_histograms->Get(nJets_reweighting_name);
  value = GetBinContent(h_nJets_reweighting, Njets);

  if(rat1 != 0) {

    TString nom_hist_name = TString::Format(histo_form_nominal, chan_name.Data(), nj_str.Data(), reweighting_var.Data());
    // cout << nom_hist_name << endl;
    if(!h_nJets_nom || h_nJets_nom->GetName() != nom_hist_name) h_nJets_nom  = (TH1D*)f_histograms->Get(nom_hist_name);
    if( h_nJets_nom == 0 ) cerr << "Error::Could not find histogram " << nom_hist_name << endl;

    TString syst_hist_name = TString::Format(histo_form_syst, chan_name.Data(), nj_str.Data(), reweighting_var.Data(), syst_map[{rat1, rat2}].c_str());
    // cout << syst_hist_name << endl;
    if(!h_nJets_sys || h_nJets_sys->GetName() != syst_hist_name) h_nJets_sys  = (TH1D*)f_histograms->Get(syst_hist_name);
    if( h_nJets_sys == 0 ) cout << "Error::Could not find histogram " << syst_hist_name << endl;

    d_ratio = GetBinContent(h_nJets_nom, Njets)/GetBinContent(h_nJets_sys, Njets);

    if((d_ratio > 99) || (d_ratio < 0.01) || (TMath::IsNaN(d_ratio))){
      d_ratio = 1.;
    }
    value *= d_ratio;

  }

  return value;
}

Double_t MyWeight_HT_all(Double_t var = 0, int Njets = 5, int nlep = 0, int rat1 = 0, int rat2 = 0){
  if(!func_HT_all){
    func_HT_all = new TF1("func_HT_all","[0]+[1]/pow(x,[2])",200,2000);
  }
  if(!f_histograms){
    f_histograms = new TFile(TRExFitter_path,"READ");
  }

  TString reweighting_var = "HT_all";
  setup_reweighting_function(func_HT_all, nlep, Njets, reweighting_var);

  value = func_HT_all->Eval(var);

  if(rat1 != 0) {

    TString nj_str = TString::Format("%i", Njets);
    int max_njets = 0;
    TString chan_name = "";
    if( nlep == 1 ) {
      max_njets = MAX_NJETS_LJETS;
      chan_name = "ljets";
    }
    else if (nlep == 2) {
      max_njets = MAX_NJETS_DIL;
      chan_name = "os2l";
    }
    else {
      cerr << "Error::MyWeight()::wrong nlep = " << nlep << endl;
      exit(1);
    }
    if(Njets >= max_njets) {
      nj_str = TString::Format("ge%i", max_njets);
    }

    TString nom_hist_name = TString::Format(histo_form_nominal, chan_name.Data(), nj_str.Data(), reweighting_var.Data());
    // cout << nom_hist_name << endl;
    if(!h_HT_all_nom || h_HT_all_nom->GetName() != nom_hist_name) h_HT_all_nom  = (TH1D*)f_histograms->Get(nom_hist_name);
    if( h_HT_all_nom == 0 ) cerr << "Error::Could not find histogram " << nom_hist_name << endl;

    TString syst_hist_name = TString::Format(histo_form_syst, chan_name.Data(), nj_str.Data(), reweighting_var.Data(), syst_map[{rat1, rat2}].c_str());
    // cout << syst_hist_name << endl;
    if(!h_HT_all_sys || h_HT_all_sys->GetName() != syst_hist_name) h_HT_all_sys  = (TH1D*)f_histograms->Get(syst_hist_name);
    if( h_HT_all_sys == 0 ) cout << "Error::Could not find histogram " << syst_hist_name << endl;

    d_ratio = GetBinContent(h_HT_all_nom, var)/GetBinContent(h_HT_all_sys, var);

    if((d_ratio > 99) || (d_ratio < 0.01) || (TMath::IsNaN(d_ratio))){
      d_ratio = 1.;
    }
    value *= d_ratio;
  }

  return value;
}

Double_t MyWeight_HT_all_red(Double_t var = 0, int Njets = 5, int nlep = 0, int rat1 = 0, int rat2 = 0){
  if(!func_HT_all_red){
    func_HT_all_red = new TF1("func_HT_all_red","[0]+[1]/pow(x,[2])",200,2000);
  }
  if(!f_histograms){
    f_histograms = new TFile(TRExFitter_path,"READ");
  }

  TString reweighting_var = "HT_all_red";
  setup_reweighting_function(func_HT_all_red, nlep, Njets, reweighting_var);

  if(var < 10.) var = 10.;
  value = func_HT_all_red->Eval(var);

  if(rat1 != 0) {

    TString nj_str = TString::Format("%i", Njets);
    int max_njets = 0;
    TString chan_name = "";
    if( nlep == 1 ) {
      max_njets = MAX_NJETS_LJETS;
      chan_name = "ljets";
    }
    else if (nlep == 2) {
      max_njets = MAX_NJETS_DIL;
      chan_name = "os2l";
    }
    else {
      cerr << "Error::MyWeight()::wrong nlep = " << nlep << endl;
      exit(1);
    }
    if(Njets >= max_njets) {
      nj_str = TString::Format("ge%i", max_njets);
    }

    TString nom_hist_name = TString::Format(histo_form_nominal, chan_name.Data(), nj_str.Data(), reweighting_var.Data());
    // cout << nom_hist_name << endl;
    if(!h_HT_all_red_nom || h_HT_all_red_nom->GetName() != nom_hist_name) h_HT_all_red_nom  = (TH1D*)f_histograms->Get(nom_hist_name);
    if( h_HT_all_red_nom == 0 ) cerr << "Error::Could not find histogram " << nom_hist_name << endl;

    TString syst_hist_name = TString::Format(histo_form_syst, chan_name.Data(), nj_str.Data(), reweighting_var.Data(), syst_map[{rat1, rat2}].c_str());
    // cout << syst_hist_name << endl;
    if(!h_HT_all_red_sys || h_HT_all_red_sys->GetName() != syst_hist_name) h_HT_all_red_sys  = (TH1D*)f_histograms->Get(syst_hist_name);
    if( h_HT_all_red_sys == 0 ) cout << "Error::Could not find histogram " << syst_hist_name << endl;

    d_ratio = GetBinContent(h_HT_all_red_nom, var)/GetBinContent(h_HT_all_red_sys, var);

    if((d_ratio > 99) || (d_ratio < 0.01) || (TMath::IsNaN(d_ratio))){
      d_ratio = 1.;
    }
    value *= d_ratio;
  }

  return value;
}


Double_t MyWeight_deltaR_avg_jets(Double_t var = 0, int Njets = 5, int nlep = 0, int rat1 = 0, int rat2 = 0){
  if(!f_histograms){
    f_histograms = new TFile(TRExFitter_path,"READ");
  }

  TString reweighting_var = "deltaR_avg_jets";

  TString nj_str = TString::Format("%i", Njets);
  int max_njets = 0;
  TString chan_name = "";
  if( nlep == 1 ) {
    max_njets = MAX_NJETS_LJETS;
    chan_name = "ljets";
  }
  else if (nlep == 2) {
    max_njets = MAX_NJETS_DIL;
    chan_name = "os2l";
  }
  else {
    cerr << "Error::MyWeight()::wrong nlep = " << nlep << endl;
    exit(1);
  }
  if(Njets >= max_njets) {
    nj_str = TString::Format("ge%i", max_njets);
  }

  TString deltaR_avg_jets_reweighting_name = TString::Format(histo_form_reweighting, chan_name.Data(), nj_str.Data(), reweighting_var.Data());
  if(!h_deltaR_avg_jets_reweighting || h_deltaR_avg_jets_reweighting->GetName() != deltaR_avg_jets_reweighting_name) h_deltaR_avg_jets_reweighting = (TH1D*)f_histograms->Get(deltaR_avg_jets_reweighting_name);

  value = GetBinContent(h_deltaR_avg_jets_reweighting, var);

  if(rat1 != 0) {

    TString nom_hist_name = TString::Format(histo_form_nominal, chan_name.Data(), nj_str.Data(), reweighting_var.Data());
    // cout << nom_hist_name << endl;
    if(!h_deltaR_avg_jets_nom || h_deltaR_avg_jets_nom->GetName() != nom_hist_name) h_deltaR_avg_jets_nom  = (TH1D*)f_histograms->Get(nom_hist_name);
    if( h_deltaR_avg_jets_nom == 0 ) cerr << "Error::Could not find histogram " << nom_hist_name << endl;

    TString syst_hist_name = TString::Format(histo_form_syst, chan_name.Data(), nj_str.Data(), reweighting_var.Data(), syst_map[{rat1, rat2}].c_str());
    // cout << syst_hist_name << endl;
    if(!h_deltaR_avg_jets_sys || h_deltaR_avg_jets_sys->GetName() != syst_hist_name) h_deltaR_avg_jets_sys  = (TH1D*)f_histograms->Get(syst_hist_name);
    if( h_deltaR_avg_jets_sys == 0 ) cout << "Error::Could not find histogram " << syst_hist_name << endl;

    d_ratio = GetBinContent(h_deltaR_avg_jets_nom, var)/GetBinContent(h_deltaR_avg_jets_sys, var);

    if((d_ratio > 99) || (d_ratio < 0.01) || (TMath::IsNaN(d_ratio))){
      d_ratio = 1.;
    }
    value *= d_ratio;
  }

  return value;
};


std::map<int, TString> dsid_map = {
  // rad  FIX ME!!!
  { 410480, "PW_PY8Sys" },
  { 410481, "PW_PY8Sys" },
  { 410482, "PW_PY8Sys" },
  // phherwig
  { 410557, "PW_H7" },
  { 410558, "PW_H7" },
  { 410559, "PW_H7" },
  // phherwig HF
  { 411082, "PW_H7" },
  { 411083, "PW_H7" },
  { 411084, "PW_H7" },
  { 411085, "PW_H7" },
  { 411086, "PW_H7" },
  { 411087, "PW_H7" },
  { 411088, "PW_H7" },
  { 411089, "PW_H7" },
  { 411090, "PW_H7" },
  // Sherpa211
  { 410249, "Sherpa" },
  { 410250, "Sherpa" },
  { 410251, "Sherpa" },
  { 410252, "Sherpa" },
  // amcnlo
  { 410464, "aMC_PY8" },
  { 410465, "aMC_PY8" },
  { 410466, "aMC_PY8" },
  // PhPy8OL_ttbb4F FIX ME!!!
  { 411068, "PW_PY8Sys" },
  { 411069, "PW_PY8Sys" },
  { 411070, "PW_PY8Sys" },
  // amcnlo
  { 412066, "aMC_PY8" },
  { 412067, "aMC_PY8" },
  { 412068, "aMC_PY8" },
  { 412069, "aMC_PY8" },
  { 412070, "aMC_PY8" },
  { 412071, "aMC_PY8" },
  { 412072, "aMC_PY8" },
  { 412073, "aMC_PY8" },
  { 412074, "aMC_PY8" },
};

std::map<TString, std::array<Double_t, 3> > frac = {
  { "h_PW_PY8_1l"	   , std::array<Double_t, 3>{ 0.0795123329992, 0.89427320988 , 0.026214457121 } } , 
  { "h_PW_PY8_2l"	   , std::array<Double_t, 3>{ 0.0636235612976, 0.909510688782, 0.0268657499199 } }, 
  { "h_PW_PY8_nonallhad"   , std::array<Double_t, 3>{ 0.0793160750886, 0.894278913284, 0.0264050116274 } }, 
  { "h_PW_PY8_allhad"	   , std::array<Double_t, 3>{ 0.107683993957 , 0.863485664991, 0.028830341052 } } , 
  { "h_aMC_PY8_1l"	   , std::array<Double_t, 3>{ 0.08347949252  , 0.890114333027, 0.0264061744534 } }, 
  { "h_aMC_PY8_2l"	   , std::array<Double_t, 3>{ 0.0585758779167, 0.917274122729, 0.0241499993547 } }, 
  { "h_aMC_PY8_allhad"	   , std::array<Double_t, 3>{ 0.107377766494 , 0.863904466819, 0.0287177666874 } }, 
  { "h_aMC_PY8_nonallhad"  , std::array<Double_t, 3>{ 0.0817409982507, 0.892010328427, 0.0262486733219 } }, 
  { "h_PW_H7_1l"	   , std::array<Double_t, 3>{ 0.0546737186682, 0.920970643634, 0.0243556376976 } }, 
  { "h_PW_H7_2l"	   , std::array<Double_t, 3>{ 0.0497275820766, 0.927527865963, 0.0227445519605 } }, 
  { "h_PW_H7_allhad"	   , std::array<Double_t, 3>{ 0.0596134587228, 0.914438977169, 0.0259475641082 } }, 
  { "h_PW_H7_nonallhad"	   , std::array<Double_t, 3>{ 0.0542458694169, 0.92153785455 , 0.0242162760334 } }, 
  { "h_Sherpa_1l"	   , std::array<Double_t, 3>{ 0.0901519944166, 0.872343432504, 0.0375045730792 } }, 
  { "h_Sherpa_2l"	   , std::array<Double_t, 3>{ 0.0811915314097, 0.883103155006, 0.0357053135839 } }, 
  { "h_Sherpa_allhad"	   , std::array<Double_t, 3>{ 0.100568668837 , 0.859780172039, 0.0396511591246 } }, 
  { "h_Sherpa_nonallhad"   , std::array<Double_t, 3>{ 0.0896324686535, 0.872967278955, 0.0374002523916 } }, 
  { "h_PW_PY8Sys_1l"	   , std::array<Double_t, 3>{ 0.0856521475369, 0.886712194126, 0.0276356583374 } }, 
  { "h_PW_PY8Sys_2l"	   , std::array<Double_t, 3>{ 0.0611977010573, 0.913242298177, 0.025560000766 } } , 
  { "h_PW_PY8Sys_allhad"   , std::array<Double_t, 3>{ 0.109091741891 , 0.861155118853, 0.0297531392556 } }, 
  { "h_PW_PY8Sys_nonallhad", std::array<Double_t, 3>{ 0.0825520462947, 0.890075427417, 0.0273725262881 } }, 
};

Double_t FracReweighting(int dsid, int nlep, int HF_class) {

  if(!dsid_map.count(dsid) or dsid == 410470 or dsid == 410471 or dsid == 410472)
    return 1.;

  TString hist_name_nom = TString::Format("h_PW_PY8_%il", nlep);
  TString hist_name = TString::Format("h_%s_%il", dsid_map[dsid].Data(), nlep);

  // calculate the ratio
  // std::cout << dsid_map[dsid][0] << std::endl;
  // std::cout << dsid_map[dsid][1] << std::endl;
  // std::cout << frac[dsid_map[dsid][0]][0] << std::endl;
  // std::cout << frac[dsid_map[dsid][0]][1] << std::endl;
  // std::cout << frac[dsid_map[dsid][0]][2] << std::endl;

  Double_t rw = frac[hist_name_nom][HF_class+1] / frac[hist_name][HF_class+1];

  return rw;

}


// std::map<int, std::array<Double_t, 3> > frac = {
//   { 410249 , std::array<Double_t, 3>{ 0.100568668837 , 0.859780172039 , 0.0396511591246 } } ,
//   { 410250 , std::array<Double_t, 3>{ 0.0902669149766 , 0.872254713869 , 0.0374783711543 } } ,
//   { 410251 , std::array<Double_t, 3>{ 0.0900632200923 , 0.872411966254 , 0.0375248136539 } } ,
//   { 410252 , std::array<Double_t, 3>{ 0.0811915314097 , 0.883103155006 , 0.0357053135839 } } ,
//   { 410480 , std::array<Double_t, 3>{ 0.0856521475369 , 0.886712194126 , 0.0276356583374 } } ,
//   { 410481 , std::array<Double_t, 3>{ 0.109091741891 , 0.861155118853 , 0.0297531392556 } } ,
//   { 410482 , std::array<Double_t, 3>{ 0.0611977010573 , 0.913242298177 , 0.025560000766 } } ,
//   { 410471 , std::array<Double_t, 3>{ 0.107683993957 , 0.863485664991 , 0.028830341052 } } ,
//   { 410466 , std::array<Double_t, 3>{ 0.107377766494 , 0.863904466819 , 0.0287177666874 } } ,
//   { 410464 , std::array<Double_t, 3>{ 0.08347949252 , 0.890114333027 , 0.0264061744534 } } ,
//   { 410465 , std::array<Double_t, 3>{ 0.0585758779167 , 0.917274122729 , 0.0241499993547 } } ,
//   { 410470 , std::array<Double_t, 3>{ 0.0793160750886 , 0.894278913284 , 0.0264050116274 } } ,
//   { 410557 , std::array<Double_t, 3>{ 0.0546737186682 , 0.920970643634 , 0.0243556376976 } } ,
//   { 410558 , std::array<Double_t, 3>{ 0.0497275820766 , 0.927527865963 , 0.0227445519605 } } ,
//   { 410559 , std::array<Double_t, 3>{ 0.0596134587228 , 0.914438977169 , 0.0259475641082 } } ,
// };

// std::map<int, std::array<int, 2> > dsid_map = {
//   // rad
//   { 410480, std::array<int, 2>{ 410470, 410480 } },
//   { 410481, std::array<int, 2>{ 410471, 410481 } },
//   { 410482, std::array<int, 2>{ 410472, 410480 } }, //410482 } },
//   // phherwig
//   { 410557, std::array<int, 2>{ 410470, 410557 } },
//   { 410558, std::array<int, 2>{ 410472, 410557 } }, //410558 } },
//   { 410559, std::array<int, 2>{ 410471, 410559 } },
//   // Sherpa211
//   { 410249, std::array<int, 2>{ 410471, 410249 } },
//   { 410250, std::array<int, 2>{ 410470, 410250 } },
//   { 410251, std::array<int, 2>{ 410470, 410251 } },
//   { 410252, std::array<int, 2>{ 410472, 410250 } }, //410252 } },
//   // amcnlo
//   { 410464, std::array<int, 2>{ 410470, 410464 } },
//   { 410465, std::array<int, 2>{ 410472, 410464 } }, //410465 } },
//   { 410466, std::array<int, 2>{ 410471, 410466 } },
//   // PhPy8OL_ttbb4F
//   { 411068, std::array<int, 2>{ 410470, 411068 } },
//   { 411069, std::array<int, 2>{ 410472, 411068 } }, //411069 } },
//   { 411070, std::array<int, 2>{ 410471, 411070 } },
//   // phherwig
//   { 411082, std::array<int, 2>{ 410470, 410557 } },
//   { 411083, std::array<int, 2>{ 410470, 410557 } },
//   { 411084, std::array<int, 2>{ 410470, 410557 } },
//   { 411085, std::array<int, 2>{ 410472, 410557 } }, //410558 } },
//   { 411086, std::array<int, 2>{ 410472, 410557 } }, //410558 } },
//   { 411087, std::array<int, 2>{ 410472, 410557 } }, //410558 } },
//   { 411088, std::array<int, 2>{ 410471, 410559 } },
//   { 411089, std::array<int, 2>{ 410471, 410559 } },
//   { 411090, std::array<int, 2>{ 410471, 410559 } },
//   // amcnlo
//   { 412066, std::array<int, 2>{ 410470, 410464 } },
//   { 412067, std::array<int, 2>{ 410470, 410464 } },
//   { 412068, std::array<int, 2>{ 410470, 410464 } },
//   { 412069, std::array<int, 2>{ 410472, 410464 } }, //410465 } },
//   { 412070, std::array<int, 2>{ 410472, 410464 } }, //410465 } },
//   { 412071, std::array<int, 2>{ 410472, 410464 } }, //410465 } },
//   { 412072, std::array<int, 2>{ 410471, 410466 } },
//   { 412073, std::array<int, 2>{ 410471, 410466 } },
//   { 412074, std::array<int, 2>{ 410471, 410466 } },
// };

// Double_t FracReweighting(int dsid, int HF_class) {

//   if(!frac.count(dsid_map[dsid][1]) or dsid == 410470 or dsid == 410471 or dsid == 410472)
//     return 1.;

//   // calculate the ratio
//   // std::cout << dsid_map[dsid][0] << std::endl;
//   // std::cout << dsid_map[dsid][1] << std::endl;
//   // std::cout << frac[dsid_map[dsid][0]][0] << std::endl;
//   // std::cout << frac[dsid_map[dsid][0]][1] << std::endl;
//   // std::cout << frac[dsid_map[dsid][0]][2] << std::endl;

//   // hard coded converter for 410472 to 410470
//   int nominal(0);
//   if( dsid_map[dsid][0] == 410472 ) nominal = 410470;
//   else nominal = dsid_map[dsid][0];

//   Double_t rw = frac[nominal][HF_class+1] / frac[dsid_map[dsid][1]][HF_class+1];

//   return rw;

// }
