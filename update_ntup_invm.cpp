#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "InvMassCalculator.hh"


using namespace std;

void update_ntup_invm(string loc = "/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/4top_lo_simon_10ji3bi.root")
//int main(int argc,char **argv)
{
  string filename = loc;
  //if (argc>1)
  //{filename = argv[1];}
  //else
  //{cout << "Please specify the file name." << endl; return 0;}

  
  TFile *f0 = new TFile((filename).c_str());

  float Mjjj_MinM,Mjjj_MaxM,Mjjj_Avg;
  float Mjjj_MindR,Mjjj_MaxdR;
  float Mjjj_MinPt,Mjjj_MaxPt;
  
  float Mbjj_MinM_MV2c10_70,Mbjj_MaxM_MV2c10_70,Mbjj_Avg_MV2c10_70;
  float Mbjj_MindR_MV2c10_70,Mbjj_MaxdR_MV2c10_70;
  float Mbjj_MinPt_MV2c10_70,Mbjj_MaxPt_MV2c10_70;

  float MbJJ_MinM_MV2c10_70,MbJJ_MaxM_MV2c10_70,MbJJ_Avg_MV2c10_70;
  float MbJJ_MindR_MV2c10_70,MbJJ_MaxdR_MV2c10_70;
  float MbJJ_MinPt_MV2c10_70,MbJJ_MaxPt_MV2c10_70;
  
  float MBjj_MinM,MBjj_MaxM,MBjj_Avg;
  float MBjj_MindR,MBjj_MaxdR;
  float MBjj_MinPt,MBjj_MaxPt;

  float MBJJ_MinM,MBJJ_MaxM,MBJJ_Avg;
  float MBJJ_MindR,MBJJ_MaxdR;
  float MBJJ_MinPt,MBJJ_MaxPt;

  cout << "Calculating Inv. Mass Variables..." << endl;
  Mass3 Mjjj = GetMass3(filename,{"jet_pt_btag_ordered","jet_pt_btag_ordered","jet_pt_btag_ordered"},{"jet_e_btag_ordered","jet_e_btag_ordered","jet_e_btag_ordered"},{"jet_phi_btag_ordered","jet_phi_btag_ordered","jet_phi_btag_ordered"},{"jet_eta_btag_ordered","jet_eta_btag_ordered","jet_eta_btag_ordered"});
  
  Mass3 Mbjj = GetMass3(filename,{"jet_pt_btag_ordered","jet_pt_btag_ordered","jet_pt_btag_ordered"},{"jet_e_btag_ordered","jet_e_btag_ordered","jet_e_btag_ordered"},{"jet_phi_btag_ordered","jet_phi_btag_ordered","jet_phi_btag_ordered"},{"jet_eta_btag_ordered","jet_eta_btag_ordered","jet_eta_btag_ordered"},1);

  Mass3 MbJJ = GetMass3(filename,{"jet_pt_btag_ordered","jet_pt_btag_ordered","jet_pt_btag_ordered"},{"jet_e_btag_ordered","jet_e_btag_ordered","jet_e_btag_ordered"},{"jet_phi_btag_ordered","jet_phi_btag_ordered","jet_phi_btag_ordered"},{"jet_eta_btag_ordered","jet_eta_btag_ordered","jet_eta_btag_ordered"},2);

  Mass3 MBjj = GetMass3(filename,{"jet_pt_btag_ordered","jet_pt_btag_ordered","jet_pt_btag_ordered"},{"jet_e_btag_ordered","jet_e_btag_ordered","jet_e_btag_ordered"},{"jet_phi_btag_ordered","jet_phi_btag_ordered","jet_phi_btag_ordered"},{"jet_eta_btag_ordered","jet_eta_btag_ordered","jet_eta_btag_ordered"},3);

  Mass3 MBJJ = GetMass3(filename,{"jet_pt_btag_ordered","jet_pt_btag_ordered","jet_pt_btag_ordered"},{"jet_e_btag_ordered","jet_e_btag_ordered","jet_e_btag_ordered"},{"jet_phi_btag_ordered","jet_phi_btag_ordered","jet_phi_btag_ordered"},{"jet_eta_btag_ordered","jet_eta_btag_ordered","jet_eta_btag_ordered"},4);

  
  cout << "\nUpdating ntuple..." << endl;
  
  TFile *f = new TFile((filename).c_str(),"update");
  TTree *T = (TTree*)f->Get("nominal_Loose");
  int event_number = T->GetEntries();
 
  TBranch *b_Mjjj_MinM  = T->Branch("Mjjj_MinM_new",&Mjjj_MinM,"Mjjj_MinM_new/F");
  TBranch *b_Mjjj_MaxM  = T->Branch("Mjjj_MaxM_new",&Mjjj_MaxM,"Mjjj_MaxM_new/F");
  TBranch *b_Mjjj_MindR = T->Branch("Mjjj_MindR_new",&Mjjj_MindR,"Mjjj_MindR_new/F");
  TBranch *b_Mjjj_MaxdR = T->Branch("Mjjj_MaxdR_new",&Mjjj_MaxdR,"Mjjj_MaxdR_new/F");
  TBranch *b_Mjjj_MinPt = T->Branch("Mjjj_MinPt_new",&Mjjj_MinPt,"Mjjj_MinPt_new/F");
  TBranch *b_Mjjj_MaxPt = T->Branch("Mjjj_MaxPt_new",&Mjjj_MaxPt,"Mjjj_MaxPt_new/F");
  TBranch *b_Mjjj_Avg   = T->Branch("Mjjj_Avg_new",&Mjjj_Avg,"Mjjj_Avg_new/F");

  TBranch *b_Mbjj_MinM  = T->Branch("Mbjj_MinM_MV2c10_70",&Mbjj_MinM_MV2c10_70,"Mbjj_MinM_MV2c10_70/F");
  TBranch *b_Mbjj_MaxM  = T->Branch("Mbjj_MaxM_MV2c10_70",&Mbjj_MaxM_MV2c10_70,"Mbjj_MaxM_MV2c10_70/F");
  TBranch *b_Mbjj_MindR = T->Branch("Mbjj_MindR_MV2c10_70",&Mbjj_MindR_MV2c10_70,"Mbjj_MindR_MV2c10_70/F");
  TBranch *b_Mbjj_MaxdR = T->Branch("Mbjj_MaxdR_MV2c10_70",&Mbjj_MaxdR_MV2c10_70,"Mbjj_MaxdR_MV2c10_70/F");
  TBranch *b_Mbjj_MinPt = T->Branch("Mbjj_MinPt_MV2c10_70",&Mbjj_MinPt_MV2c10_70,"Mbjj_MinPt_MV2c10_70/F");
  TBranch *b_Mbjj_MaxPt = T->Branch("Mbjj_MaxPt_MV2c10_70",&Mbjj_MaxPt_MV2c10_70,"Mbjj_MaxPt_MV2c10_70/F");
  TBranch *b_Mbjj_Avg   = T->Branch("Mbjj_Avg_MV2c10_70",&Mbjj_Avg_MV2c10_70,"Mbjj_Avg_MV2c10_70/F");

  TBranch *b_MbJJ_MinM  = T->Branch("MbJJ_MinM_MV2c10_70",&MbJJ_MinM_MV2c10_70,"MbJJ_MinM_MV2c10_70/F");
  TBranch *b_MbJJ_MaxM  = T->Branch("MbJJ_MaxM_MV2c10_70",&MbJJ_MaxM_MV2c10_70,"MbJJ_MaxM_MV2c10_70/F");
  TBranch *b_MbJJ_MindR = T->Branch("MbJJ_MindR_MV2c10_70",&MbJJ_MindR_MV2c10_70,"MbJJ_MindR_MV2c10_70/F");
  TBranch *b_MbJJ_MaxdR = T->Branch("MbJJ_MaxdR_MV2c10_70",&MbJJ_MaxdR_MV2c10_70,"MbJJ_MaxdR_MV2c10_70/F");
  TBranch *b_MbJJ_MinPt = T->Branch("MbJJ_MinPt_MV2c10_70",&MbJJ_MinPt_MV2c10_70,"MbJJ_MinPt_MV2c10_70/F");
  TBranch *b_MbJJ_MaxPt = T->Branch("MbJJ_MaxPt_MV2c10_70",&MbJJ_MaxPt_MV2c10_70,"MbJJ_MaxPt_MV2c10_70/F");
  TBranch *b_MbJJ_Avg   = T->Branch("MbJJ_Avg_MV2c10_70",&MbJJ_Avg_MV2c10_70,"MbJJ_Avg_MV2c10_70/F");

  TBranch *b_MBjj_MinM  = T->Branch("MBjj_MinM",&MBjj_MinM,"MBjj_MinM/F");
  TBranch *b_MBjj_MaxM  = T->Branch("MBjj_MaxM",&MBjj_MaxM,"MBjj_MaxM/F");
  TBranch *b_MBjj_MindR = T->Branch("MBjj_MindR",&MBjj_MindR,"MBjj_MindR/F");
  TBranch *b_MBjj_MaxdR = T->Branch("MBjj_MaxdR",&MBjj_MaxdR,"MBjj_MaxdR/F");
  TBranch *b_MBjj_MinPt = T->Branch("MBjj_MinPt",&MBjj_MinPt,"MBjj_MinPt/F");
  TBranch *b_MBjj_MaxPt = T->Branch("MBjj_MaxPt",&MBjj_MaxPt,"MBjj_MaxPt/F");
  TBranch *b_MBjj_Avg   = T->Branch("MBjj_Avg",&MBjj_Avg,"MBjj_Avg/F");

  TBranch *b_MBJJ_MinM  = T->Branch("MBJJ_MinM",&MBJJ_MinM,"MBJJ_MinM/F");
  TBranch *b_MBJJ_MaxM  = T->Branch("MBJJ_MaxM",&MBJJ_MaxM,"MBJJ_MaxM/F");
  TBranch *b_MBJJ_MindR = T->Branch("MBJJ_MindR",&MBJJ_MindR,"MBJJ_MindR/F");
  TBranch *b_MBJJ_MaxdR = T->Branch("MBJJ_MaxdR",&MBJJ_MaxdR,"MBJJ_MaxdR/F");
  TBranch *b_MBJJ_MinPt = T->Branch("MBJJ_MinPt",&MBJJ_MinPt,"MBJJ_MinPt/F");
  TBranch *b_MBJJ_MaxPt = T->Branch("MBJJ_MaxPt",&MBJJ_MaxPt,"MBJJ_MaxPt/F");
  TBranch *b_MBJJ_Avg   = T->Branch("MBJJ_Avg",&MBJJ_Avg,"MBJJ_Avg/F");


  for (int i=0;i<event_number;i++){

    Mjjj_MinM    = Mjjj[i].MinM;
    Mjjj_MaxM    = Mjjj[i].MaxM;
    Mjjj_MindR   = Mjjj[i].MindR;
    Mjjj_MaxdR   = Mjjj[i].MaxdR;
    Mjjj_MinPt   = Mjjj[i].MinPt;
    Mjjj_MaxPt   = Mjjj[i].MaxPt;
    Mjjj_Avg     = Mjjj[i].Avg;
    
    Mbjj_MinM_MV2c10_70    = Mbjj[i].MinM;
    Mbjj_MaxM_MV2c10_70    = Mbjj[i].MaxM;
    Mbjj_MindR_MV2c10_70   = Mbjj[i].MindR;
    Mbjj_MaxdR_MV2c10_70   = Mbjj[i].MaxdR;
    Mbjj_MinPt_MV2c10_70   = Mbjj[i].MinPt;
    Mbjj_MaxPt_MV2c10_70   = Mbjj[i].MaxPt;
    Mbjj_Avg_MV2c10_70     = Mbjj[i].Avg;
          		    
    MbJJ_MinM_MV2c10_70    = MbJJ[i].MinM;
    MbJJ_MaxM_MV2c10_70    = MbJJ[i].MaxM;
    MbJJ_MindR_MV2c10_70   = MbJJ[i].MindR;
    MbJJ_MaxdR_MV2c10_70   = MbJJ[i].MaxdR;
    MbJJ_MinPt_MV2c10_70   = MbJJ[i].MinPt;
    MbJJ_MaxPt_MV2c10_70   = MbJJ[i].MaxPt;
    MbJJ_Avg_MV2c10_70     = MbJJ[i].Avg;
    
    MBjj_MinM    = MBjj[i].MinM;
    MBjj_MaxM    = MBjj[i].MaxM;
    MBjj_MindR   = MBjj[i].MindR;
    MBjj_MaxdR   = MBjj[i].MaxdR;
    MBjj_MinPt   = MBjj[i].MinPt;
    MBjj_MaxPt   = MBjj[i].MaxPt;
    MBjj_Avg     = MBjj[i].Avg;
          		    
    MBJJ_MinM    = MBJJ[i].MinM;
    MBJJ_MaxM    = MBJJ[i].MaxM;
    MBJJ_MindR   = MBJJ[i].MindR;
    MBJJ_MaxdR   = MBJJ[i].MaxdR;
    MBJJ_MinPt   = MBJJ[i].MinPt;
    MBJJ_MaxPt   = MBJJ[i].MaxPt;
    MBJJ_Avg     = MBJJ[i].Avg;
    
    b_Mjjj_MinM->Fill(); 
    b_Mjjj_MaxM->Fill(); 
    b_Mjjj_MindR->Fill(); 
    b_Mjjj_MaxdR->Fill();     
    b_Mjjj_MinPt->Fill(); 
    b_Mjjj_MaxPt->Fill(); 
    b_Mjjj_Avg->Fill();   
                 
    b_Mbjj_MinM->Fill();  
    b_Mbjj_MaxM->Fill();  
    b_Mbjj_MindR->Fill(); 
    b_Mbjj_MaxdR->Fill(); 
    b_Mbjj_MinPt->Fill(); 
    b_Mbjj_MaxPt->Fill(); 
    b_Mbjj_Avg->Fill();   
            
    b_MbJJ_MinM->Fill();  
    b_MbJJ_MaxM->Fill();  
    b_MbJJ_MindR->Fill(); 
    b_MbJJ_MaxdR->Fill(); 
    b_MbJJ_MinPt->Fill(); 
    b_MbJJ_MaxPt->Fill(); 
    b_MbJJ_Avg->Fill(); 
          
    b_MBjj_MinM->Fill();  
    b_MBjj_MaxM->Fill();  
    b_MBjj_MindR->Fill(); 
    b_MBjj_MaxdR->Fill(); 
    b_MBjj_MinPt->Fill(); 
    b_MBjj_MaxPt->Fill(); 
    b_MBjj_Avg->Fill();   
            
    b_MBJJ_MinM->Fill();  
    b_MBJJ_MaxM->Fill();  
    b_MBJJ_MindR->Fill(); 
    b_MBJJ_MaxdR->Fill(); 
    b_MBJJ_MinPt->Fill(); 
    b_MBJJ_MaxPt->Fill(); 
    b_MBJJ_Avg->Fill(); 
  }

  T->Write("nominal_Loose");

  delete f;
  
}
