NAME=update_ntup_latest
EXEC=$(NAME)

CXX=g++
CINT=rootcint
CFLAGS=-I/usr/include/root
LDFLAGS=-O -Wall `root-config --cflags --libs` -lTMVA -lm -lz 
SOURCE=$(NAME).cpp


$(EXEC): $(SOURCE)
	$(CXX) $(CFLAGS) $(LDFLAGS) $(SOURCE) -o $(EXEC)

clean:
	@rm -fv $(EXEC)
