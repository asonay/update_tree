#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLorentzVector.h"

#include "TSystem.h"
#include "TChain.h"
#include "TCut.h"
#include "TDirectory.h"
#include "TH1F.h"
#include "TH1.h"
#include "TMath.h"
#include "TFile.h"
#include "TStopwatch.h"
#include "TROOT.h"

#include "TMVA/GeneticAlgorithm.h"
#include "TMVA/GeneticFitter.h"
#include "TMVA/IFitterTarget.h"
#include "TMVA/Factory.h"
#include "TMVA/Reader.h"

#include "TTreeReader.h"
#include "TTreeReaderValue.h"

#include "Variables.hpp"


using namespace std;
using namespace TMVA;

//void update_ntup_invm(string loc = "/home/asonay/ATLAS/MVA_result/SM4t-212560_ntuples/offline/1LOS/mvatuple_7ji2bi/4top_lo_simon_10ji3bi.root")
int main(int argc,char **argv)
{
  string filename;
  string channel;
  string jet_mul;
  if (argc>1)
    {filename = argv[1];}
  else
    {cout << "Please specify the file name." << endl; return 0;}
  if (argc>2)
    {channel = argv[2];}
  else
    {cout << "Please specify the channel." << endl; return 0;}
  if (argc>3)
    {jet_mul = argv[3];}
  else
    {cout << "Please specify the using jet mult." << endl; return 0;}

  TString weight_file1 ;
  TString weight_file2 ;
  TString weight_file3 ;

  TString method = "BDT method";

  if (channel == "1l"){
    weight_file1 = "/eos/atlas/atlascerngroupdisk/phys-top/4tops2019/BDTXMLs/BDT_050919_1l_set13/TMVA_4tops_1l_"+jet_mul+"3bi_cross_val_sample_0_BDTG.weights.xml";
    weight_file2 = "/eos/atlas/atlascerngroupdisk/phys-top/4tops2019/BDTXMLs/BDT_050919_1l_set13/TMVA_4tops_1l_"+jet_mul+"3bi_cross_val_sample_1_BDTG.weights.xml";
    weight_file3 = "/eos/atlas/atlascerngroupdisk/phys-top/4tops2019/BDTXMLs/BDT_050919_1l_set13/TMVA_4tops_1l_"+jet_mul+"3bi_cross_val_sample_2_BDTG.weights.xml";
  }
  if (channel == "2l"){
    weight_file1 = "/eos/atlas/atlascerngroupdisk/phys-top/4tops2019/BDTXMLs/BDT_050919_2l_set13/TMVA_4tops_2l_"+jet_mul+"3bi_cross_val_sample_0_BDTG.weights.xml";
    weight_file2 = "/eos/atlas/atlascerngroupdisk/phys-top/4tops2019/BDTXMLs/BDT_050919_2l_set13/TMVA_4tops_2l_"+jet_mul+"3bi_cross_val_sample_1_BDTG.weights.xml";
    weight_file3 = "/eos/atlas/atlascerngroupdisk/phys-top/4tops2019/BDTXMLs/BDT_050919_2l_set13/TMVA_4tops_2l_"+jet_mul+"3bi_cross_val_sample_2_BDTG.weights.xml";
  }
  
  vector<string> variables = variables_1l_njet;
  if (jet_mul == "10ji" || jet_mul == "8ji"){
    if (channel == "1l")
      variables = variables_1l_njet;
    else if (channel == "2l")
      variables = variables_2l_njet;
  }
  else {
    if (channel == "1l")
      variables = variables_1l;
    else if (channel == "2l")
      variables = variables_2l;
  }
  
  TFile *f = new TFile((filename).c_str(),"update");
  
  TTreeReader reader("nominal_Loose",f);
  
  TTreeReaderValue<vector<float>> pt(reader, "jet_pt_btag_ordered");
  TTreeReaderValue<vector<float>> et(reader, "jet_e_btag_ordered");
  TTreeReaderValue<vector<float>> eta(reader, "jet_eta_btag_ordered");
  TTreeReaderValue<vector<float>> phi(reader, "jet_phi_btag_ordered");

  TTreeReaderValue<int> en(reader, "eventNumber");
  TTreeReaderValue<float> HT_all(reader, "HT_all");
  TTreeReaderValue<vector<float>> jet_pt(reader, "jet_pt");
  TTreeReaderValue<int> nJets(reader, "nJets");
  TTreeReaderValue<float> Centrality_all(reader, "Centrality_all");
  TTreeReaderValue<float> dRjj_Avg(reader, "dRjj_Avg");
  TTreeReaderValue<float> dRbb_MindR_MV2c10_70(reader, "dRbb_MindR_MV2c10_70");
  TTreeReaderValue<float> dRbl_MindR_MV2c10_70(reader, "dRbl_MindR_MV2c10_70");
  TTreeReaderValue<float> Mbbb_Avg_MV2c10_70(reader, "Mbbb_Avg_MV2c10_70");
  TTreeReaderValue<float> Mbb_MinM_MV2c10_70(reader, "Mbb_MinM_MV2c10_70");
  TTreeReaderValue<int> nRCJetsM100(reader, "nRCJetsM100");
  TTreeReaderValue<vector<float>> rcjet_d12(reader, "rcjet_d12");
  TTreeReaderValue<vector<float>> rcjet_d23(reader, "rcjet_d23");
  TTreeReaderValue<float> mtw(reader, "mtw");
  TTreeReaderValue<float> met_met(reader, "met_met");
  TTreeReaderValue<vector<float>> jet_pcb_MV2c10_btag_ordered(reader, "jet_pcb_MV2c10_btag_ordered");

  float Mjjj_MindR,BDT_13;
  
  TTree *T = (TTree*)f->Get("nominal_Loose");
  int event_number = T->GetEntries();
 
  TBranch *b_Mjjj_MindR = T->Branch("Mjjj_MindR",&Mjjj_MindR,"Mjjj_MindR_new/F");
  TBranch *b_BDT_13 = T->Branch("BDT_13",&BDT_13,"BDT_13/F");

  TLorentzVector vec,vec1,vec2,vec3;
  
  float min_deltaR = FLT_MAX;
  float min_trijet_mass = 0;
  float current_trijet_mass = 0;
  float current_deltaR = 0;

  float sum_rcjet_d12=0;
  float sum_rcjet_d23=0;
  float sum_pcb=0;

  TMVA::Reader *reader1 = new TMVA::Reader( "!Color:!Silent" );
  TMVA::Reader *reader2 = new TMVA::Reader( "!Color:!Silent" );
  TMVA::Reader *reader3 = new TMVA::Reader( "!Color:!Silent" );

  const int n = (int)variables.size();
  Float_t local_variables[n];
  
  for(int i=0;i<n;i++){
    reader1->AddVariable(variables[i].c_str() , &local_variables[i] );
    reader2->AddVariable(variables[i].c_str() , &local_variables[i] );
    reader3->AddVariable(variables[i].c_str() , &local_variables[i] );
  }
  
  reader1->BookMVA( method, weight_file1 );
  reader2->BookMVA( method, weight_file2 );
  reader3->BookMVA( method, weight_file3 );

  for (int l=0;l<event_number;l++){

    min_deltaR = FLT_MAX;
    min_trijet_mass = 0;
    current_trijet_mass = 0;
    current_deltaR = 0;
    
    for (int i=0;i<(int)(*et).size();i++){
      for (int j=0;j<(int)(*et).size();j++){
	for (int k=0;k<(int)(*et).size();k++){
	  if(i != k && i!=j && j != k){
	    vec1.SetPtEtaPhiE((*pt)[i],(*eta)[i],(*phi)[i],(*et)[i]);
	    vec2.SetPtEtaPhiE((*pt)[j],(*eta)[j],(*phi)[j],(*et)[j]);
	    vec3.SetPtEtaPhiE((*pt)[k],(*eta)[k],(*phi)[k],(*et)[k]);
	  
	    vec = vec1+vec2+vec3;
	  
	    current_trijet_mass = vec.M();
	    current_deltaR = sqrt(pow(vec1.DeltaR(vec2),2) + pow(vec1.DeltaR(vec3),2) + pow(vec2.DeltaR(vec3),2));
	  
	    if(current_deltaR < min_deltaR){
	      min_deltaR=current_deltaR;
	      min_trijet_mass = current_trijet_mass;
	    }
	  }
	}
      }
    }

    Mjjj_MindR   = min_trijet_mass;

    sum_rcjet_d12=0.0;
    for (int i=0;i<(int)(*rcjet_d12).size();i++)
      sum_rcjet_d12+=(*rcjet_d12)[i];
    sum_rcjet_d23=0.0;
    for (int i=0;i<(int)(*rcjet_d23).size();i++)
      sum_rcjet_d23+=(*rcjet_d23)[i];
    sum_pcb=0.0;
    for (int i=0;i<(int)(*jet_pcb_MV2c10_btag_ordered).size();i++)
      if (i<6)
	sum_pcb+=(*jet_pcb_MV2c10_btag_ordered)[i];

    local_variables[0] =  *HT_all;
    local_variables[1] =  (*jet_pt)[0];
    if (jet_mul=="10ji" || jet_mul=="8ji"){
      local_variables[2] =  *nJets;
      local_variables[3] =  *Centrality_all;
      local_variables[4] =  *dRjj_Avg;
      local_variables[5] =  *dRbb_MindR_MV2c10_70;
      local_variables[6] =  Mjjj_MindR;
      local_variables[7] =  *Mbbb_Avg_MV2c10_70;
      local_variables[8] =  *Mbb_MinM_MV2c10_70;
      local_variables[9] =  *nRCJetsM100;
      local_variables[10] =  sum_rcjet_d12;
      local_variables[11] =  sum_rcjet_d23;
      if (channel=="1l"){
	local_variables[12] =  *mtw;
	local_variables[13] =  *met_met;
	local_variables[14] =  sum_pcb;
      }
      else {
	local_variables[12] =  *met_met;
	local_variables[13] =  sum_pcb;
      }
    }
    else {
      local_variables[2] =  *Centrality_all;
      local_variables[3] =  *dRjj_Avg;
      local_variables[4] =  *dRbb_MindR_MV2c10_70;
      local_variables[5] =  Mjjj_MindR;
      local_variables[6] =  *Mbbb_Avg_MV2c10_70;
      local_variables[7] =  *Mbb_MinM_MV2c10_70;
      local_variables[8] =  *nRCJetsM100;
      local_variables[9] =  sum_rcjet_d12;
      local_variables[10] =  sum_rcjet_d23;
      if (channel=="1l"){
	local_variables[11] =  *mtw;
	local_variables[12] =  *met_met;
	local_variables[13] =  sum_pcb;
      }
      else {
	local_variables[11] =  *met_met;
	local_variables[12] =  sum_pcb;
      }
    }

    if ((*en)%3==2)
      BDT_13 = reader1->EvaluateMVA( method );
    if ((*en)%3==1)
      BDT_13 = reader2->EvaluateMVA( method );
    if ((*en)%3==0)
      BDT_13 = reader3->EvaluateMVA( method );
    
    b_Mjjj_MindR->Fill(); 
    b_BDT_13->Fill(); 

  }

  T->Write("nominal_Loose");

  delete f;
  
}
