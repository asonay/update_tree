#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TH1F.h"
#include "TMath.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"
//#include "TLorenzVector.h"

using namespace std;

struct KinematicValues
{
  float m;
  float dphi;
  float deta;
  float dR;
  float pt;
};

struct InvMass3x
{
  float MaxdR;
  float MindR;
  
  float MaxPt;
  float MinPt;
  
  float MaxM;
  float MinM;
  
  float Avg;
};

typedef vector<KinematicValues> Kinematics;
typedef vector<InvMass3x> Mass3;



float get_dphi(pair<float,float> x_phi){
  float dphi = 0;
  float mpi = TMath::Pi();
  
  dphi = fabs( x_phi.first - x_phi.second );
  if(dphi>mpi)
    dphi = 2*mpi - dphi;
  
  return dphi;
  
}

float get_deta(pair<float,float> x_eta){

  float deta = 0;

  deta = fabs( x_eta.first - x_eta.second );
    
  return deta;
  
}

float get_dR2(pair<float,float> x_eta,pair<float,float> x_phi){
  
  float dR2 = 0;
  float mpi = TMath::Pi();

  float dphi = fabs( x_phi.first - x_phi.second );
  float deta = fabs( x_eta.first - x_eta.second );
  if(dphi>mpi)
    dphi = 2*mpi - dphi;

  dR2 = 100 * ( (dphi)*(dphi) + (deta)*(deta) );

  return dR2;
  
}

float get_inv_mass2_2x(pair<float,float> x_pt,pair<float,float> x_eta,pair<float,float> x_phi){

  float invmass2 = 0;
  float mpi = TMath::Pi();

 
  float deta = fabs( x_eta.first - x_eta.second );
  float dphi = fabs( x_phi.first - x_phi.second );
  if(dphi>mpi)
    dphi = 2*mpi - dphi;

  float cosheta = cosh ( deta);
  float cosphi = cos ( dphi);
  invmass2 = 2*x_pt.first*x_pt.second*(cosheta - cosphi);


  return invmass2;
  
}


Kinematics GetKinematics(string file,pair<string,string> Et,pair<string,string> phi,pair<string,string> eta, string reqsb = "max", string sortby = "mass")
{
  TFile *f0 = new TFile((file).c_str());
  TTreeReader reader("nominal_Loose",f0);

  Kinematics x,xs;
  
  TTreeReaderValue<vector<float>> et1_sel(reader, Et.first.c_str());
  TTreeReaderValue<vector<float>> et2_sel(reader, Et.second.c_str());
  TTreeReaderValue<vector<float>> eta1(reader, eta.first.c_str());
  TTreeReaderValue<vector<float>> eta2(reader, eta.second.c_str());
  TTreeReaderValue<vector<float>> phi1(reader, phi.first.c_str());
  TTreeReaderValue<vector<float>> phi2(reader, phi.second.c_str());
  
  
  while(reader.Next()){
    

    xs.clear();
    //cout << "" << endl;
    for (int i=0;i<(int)(*et1_sel).size();i++){
      for (int j=0;j<(int)(*et2_sel).size();j++){

	if ( (*eta1)[i]==(*eta2)[j] && (*phi1)[i]==(*phi2)[j] ) continue;

	float m_x = get_inv_mass2_2x({(*et1_sel)[i],(*et2_sel)[j]},
				     {(*eta1)[i],(*eta2)[j]},
				     {(*phi1)[i],(*phi2)[j]}
				     );
	float dR_x = get_dR2({(*eta1)[i],(*eta2)[j]},
			     {(*phi1)[i],(*phi2)[j]}
			     );
	float deta_x = get_deta({(*eta1)[i],(*eta2)[j]});
	float dphi_x = get_dphi({(*phi1)[i],(*phi2)[j]});

	xs.push_back(KinematicValues());
	xs.back().m = m_x;
	xs.back().dR = dR_x;
	xs.back().deta = deta_x;
	xs.back().dphi = dphi_x;

      }
    }
    
    if (xs.size()>0){

      if (sortby == "mass")
	sort(xs.begin(),xs.end(),
	     [](auto const &a, auto const &b) { return a.m < b.m; });
      else if (sortby == "dphi")
	sort(xs.begin(),xs.end(),
	     [](auto const &a, auto const &b) { return a.dphi < b.dphi; });
      else if (sortby == "deta")
	sort(xs.begin(),xs.end(),
	     [](auto const &a, auto const &b) { return a.deta < b.deta; });
      else if (sortby == "dR")
	sort(xs.begin(),xs.end(),
	     [](auto const &a, auto const &b) { return a.dR < b.dR; });
      else {
	cout << "Possible options are: mass, dphi, deta, dR. Please ensure one of them." << endl;
	break;
      }
 
      x.push_back(KinematicValues());
      if (reqsb == "min"){
	x.back().m = sqrt(xs[0].m);
	x.back().dR = sqrt(xs[0].dR);
	x.back().deta = xs[0].deta;
	x.back().dphi = xs[0].dphi;
      }
      else if (reqsb == "max"){
	x.back().m = sqrt(xs[(int)xs.size()-1].m);
	x.back().dR = sqrt(xs[(int)xs.size()-1].dR);
	x.back().deta = xs[(int)xs.size()-1].deta;
	x.back().dphi = xs[(int)xs.size()-1].dphi;
      }
      else if (reqsb == "avg"){
	float sum = 0;
	for (int i=0;i<(int)xs.size();i++)
	  sum+=xs[i].m/(float)xs.size();
	x.back().m = sqrt(sum);
	sum = 0;
	for (int i=0;i<(int)xs.size();i++)
	  sum+=xs[i].dR/(float)xs.size();
	x.back().dR = sqrt(sum);
	sum = 0;
	for (int i=0;i<(int)xs.size();i++)
	  sum+=xs[i].deta/(float)xs.size();
	x.back().deta = sum;
	sum = 0;
	for (int i=0;i<(int)xs.size();i++)
	  sum+=xs[i].dphi/(float)xs.size();
	x.back().dphi = sum;
      }

    }
  }
  
  reader.Restart();
  
  return x;
}

Mass3 GetMass3(string file,vector<string> Pt, vector<string> Et,vector<string> phi,vector<string> eta,int opt=-1)
{

  Mass3 x;
  Kinematics xs;

  if (Et.size()!=3) {cout << "You must define 3 kinematic!" << endl; exit(0);}
  
  TFile *f0 = new TFile((file).c_str());
  TTreeReader reader("nominal_Loose",f0);
  
  TTreeReaderValue<vector<float>> pt1_sel(reader, Pt[0].c_str());
  TTreeReaderValue<vector<float>> pt2_sel(reader, Pt[1].c_str());
  TTreeReaderValue<vector<float>> pt3_sel(reader, Pt[2].c_str());
  TTreeReaderValue<vector<float>> et1_sel(reader, Et[0].c_str());
  TTreeReaderValue<vector<float>> et2_sel(reader, Et[1].c_str());
  TTreeReaderValue<vector<float>> et3_sel(reader, Et[2].c_str());
  TTreeReaderValue<vector<float>> eta1(reader, eta[0].c_str());
  TTreeReaderValue<vector<float>> eta2(reader, eta[1].c_str());
  TTreeReaderValue<vector<float>> eta3(reader, eta[2].c_str());
  TTreeReaderValue<vector<float>> phi1(reader, phi[0].c_str());
  TTreeReaderValue<vector<float>> phi2(reader, phi[1].c_str());
  TTreeReaderValue<vector<float>> phi3(reader, phi[2].c_str());

  TTreeReaderValue<vector<float>> btag(reader, "jet_discrete_MV2c10_btag_ordered");

  TLorentzVector vec,vec1,vec2,vec3;
  
  int ent = reader.GetEntries();
  int count = 0;
  string scount = ">";
  string scountc = scount;
  cout << "" << endl;
  while(reader.Next()){

     if (count%(ent/10)==0){
      cout << "\r|| " << scount << " " << setw(3) << (int)(100*(((double)count/(double)ent)))+1 << "% completed." << flush;
      scount += scountc;
    }
    count++;
   
    xs.clear();
    for (int i=0;i<(int)(*et1_sel).size();i++){
      for (int j=0;j<(int)(*et2_sel).size();j++){
	for (int k=0;k<(int)(*et3_sel).size();k++){

	  if (!(i!=j && i!=k && j!=k)) continue;

	  if (opt==0)
	    if ( (*btag)[i]<0.85 || (*btag)[j]<0.85 || (*btag)[k]<0.85 ) continue;
	  
	  if (opt==1)
	    if ( (*btag)[i]<0.85 ) continue;

	  if (opt==2)
	    if ( !( (*btag)[i]>0.85 && (*btag)[j]<0.85 && (*btag)[k]<0.85 ) ) continue;

	  if (opt==3)
	    if ( !(i<4 && (*btag)[i]>0.85) ) continue;

	  if (opt==4)
	    if ( !( (i<4 && (*btag)[i]>0.85) && (*btag)[j]<0.85 && (*btag)[k]<0.85) ) continue;


	  
	  vec1.SetPtEtaPhiE((*pt1_sel)[i],(*eta1)[i],(*phi1)[i],(*et1_sel)[i]);
	  vec2.SetPtEtaPhiE((*pt2_sel)[j],(*eta2)[j],(*phi2)[j],(*et2_sel)[j]);
	  vec3.SetPtEtaPhiE((*pt3_sel)[k],(*eta3)[k],(*phi3)[k],(*et3_sel)[k]);

	  vec = vec1+vec2+vec3;

	  xs.push_back(KinematicValues());
	  xs.back().m = vec.M();
	  xs.back().dR = sqrt(pow(vec1.DeltaR(vec2),2) + pow(vec1.DeltaR(vec3),2) + pow(vec2.DeltaR(vec3),2));
	  xs.back().pt = (*et1_sel)[i]+(*et2_sel)[j]+(*et3_sel)[k];

	}
      }
    }
    
    if (xs.size()>0){

      x.push_back(InvMass3x());
      
      sort(xs.begin(),xs.end(),
	   [](auto const &a, auto const &b) { return a.m < b.m; });
      x.back().MinM = xs[0].m;
      x.back().MaxM = xs[(int)xs.size()-1].m;
	
      sort(xs.begin(),xs.end(),
	   [](auto const &a, auto const &b) { return a.dR < b.dR; });
      x.back().MindR = xs[0].m;
      x.back().MaxdR = xs[(int)xs.size()-1].m;
      
      sort(xs.begin(),xs.end(),
	   [](auto const &a, auto const &b) { return a.pt < b.pt; });
      x.back().MinPt = xs[0].m;
      x.back().MaxPt = xs[(int)xs.size()-1].m;
	
      float sum = 0;
      for (int i=0;i<(int)xs.size();i++)
	sum+=xs[i].m/(float)xs.size();
      x.back().Avg = sum;
    }
    else{
      x.push_back(InvMass3x());
      x.back().MinM = 0;
      x.back().MaxM = 0;
      x.back().MindR = 0;
      x.back().MaxdR = 0;
      x.back().MinPt = 0;
      x.back().MaxPt = 0;
      x.back().Avg = 0;
    }
      
  }
  cout << "" << endl;
  reader.Restart();
 
  return x;
}

