#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "MyWeight.C"


using namespace std;

void update_ntup(string loc,int nl,bool rw_var = true)
//int main(int argc,char **argv)
{
  string filename = loc;
  //if (argc>1)
  //{filename = argv[1];}
  //else
  //{cout << "Please specify the file name." << endl; return 0;}

  
  TFile *f0 = new TFile((filename).c_str(),"update");

  float rw_ht_red,rw_njet,rw_dr;
  vector<float> w_ht_red,w_njets,w_dr;

  TTreeReader reader("nominal_Loose",f0);
  
  TTreeReaderValue<float> HT_all(reader, "HT_all");
  TTreeReaderValue<float> deltaR_avg_jets(reader, "deltaR_avg_jets");
  TTreeReaderValue<int> nJets(reader, "nJets");

  
  while(reader.Next()){
    rw_ht_red=1.0;rw_njet=1.0;rw_dr=1.0;
    if (rw_var){
      rw_ht_red = MyWeight_HT_all_red(((*HT_all/1000)-((*nJets-7*(nl==1)-5*(nl==2))*90)), *nJets, nl, 0, 0);
      rw_njet = MyWeight_nJets(*nJets,*nJets,nl);
      rw_dr = MyWeight_deltaR_avg_jets(*deltaR_avg_jets,*nJets,nl);
    }
    w_ht_red.push_back(rw_ht_red);
    w_njets.push_back(rw_njet);
    w_dr.push_back(rw_dr);
  }
  f0->Close();

  TFile *f = new TFile((filename).c_str(),"update");
  TTreeReader reader_next("nominal_Loose",f);
  TTree *T = (TTree*)f->Get("nominal_Loose");
  int event_number = 0;
 
  TBranch *b_rw_ht_red = T->Branch("rw_ht_red",&rw_ht_red,"rw_ht_red/F");
  TBranch *b_rw_njet = T->Branch("rw_njet",&rw_njet,"rw_njet/F");
  TBranch *b_rw_dr = T->Branch("rw_dr",&rw_dr,"rw_dr/F");
  TBranch *b_event_number = T->Branch("event_number",&event_number,"event_number/I");

  while(reader_next.Next()){
    rw_ht_red=w_ht_red[event_number];
    rw_njet=w_njets[event_number];
    rw_dr=w_dr[event_number];

    b_rw_ht_red->Fill();
    b_rw_njet->Fill();
    b_rw_dr->Fill();
    b_event_number->Fill();
    event_number++;
  }

  T->Print();
  T->Write("nominal_Loose");

  delete f;

}
