#include <vector>

using namespace std;
vector<string> variables_1l_njet = {
				    "HT_all"      ,
				    "jet_pt[0]"      ,
				    "nJets"       ,
				    "Centrality_all"  , 
				    "dRjj_Avg"          ,
				    "dRbb_MindR_MV2c10_70"   ,
				    "dRbl_MindR_MV2c10_70"   ,
				    "Mjjj_MindR_new"   ,
				    "Mbbb_Avg_MV2c10_70"  ,
				    "Mbb_MinM_MV2c10_70"  ,
				    "nRCJetsM100"      ,
				    "Sum$(rcjet_d12)"  ,
				    "Sum$(rcjet_d23)"  ,
				    "mtw"    ,
				    "met_met",
				    "Sum$(jet_pcb_MV2c10_btag_ordered*(Iteration$<6))",
};

vector<string> variables_1l = {
				    "HT_all"      ,
				    "jet_pt[0]"      ,
				    "Centrality_all"  , 
				    "dRjj_Avg"          ,
				    "dRbb_MindR_MV2c10_70"   ,
				    "dRbl_MindR_MV2c10_70"   ,
				    "Mjjj_MindR_new"   ,
				    "Mbbb_Avg_MV2c10_70"  ,
				    "Mbb_MinM_MV2c10_70"  ,
				    "nRCJetsM100"      ,
				    "Sum$(rcjet_d12)"  ,
				    "Sum$(rcjet_d23)"  ,
				    "mtw"    ,
				    "met_met",
				    "Sum$(jet_pcb_MV2c10_btag_ordered*(Iteration$<6))",
};

vector<string> variables_2l_njet = {
				    "HT_all"      ,
				    "jet_pt[0]"      ,
				    "nJets"       ,
				    "Centrality_all"  , 
				    "dRjj_Avg"          ,
				    "dRbb_MindR_MV2c10_70"   ,
				    "dRbl_MindR_MV2c10_70"   ,
				    "Mjjj_MindR_new"   ,
				    "Mbbb_Avg_MV2c10_70"  ,
				    "Mbb_MinM_MV2c10_70"  ,
				    "nRCJetsM100"      ,
				    "Sum$(rcjet_d12)"  ,
				    "Sum$(rcjet_d23)"  ,
				    "met_met",
				    "Sum$(jet_pcb_MV2c10_btag_ordered*(Iteration$<6))",
};

vector<string> variables_2l = {
				    "HT_all"      ,
				    "jet_pt[0]"      ,
				    "Centrality_all"  , 
				    "dRjj_Avg"          ,
				    "dRbb_MindR_MV2c10_70"   ,
				    "dRbl_MindR_MV2c10_70"   ,
				    "Mjjj_MindR_new"   ,
				    "Mbbb_Avg_MV2c10_70"  ,
				    "Mbb_MinM_MV2c10_70"  ,
				    "nRCJetsM100"      ,
				    "Sum$(rcjet_d12)"  ,
				    "Sum$(rcjet_d23)"  ,
				    "met_met",
				    "Sum$(jet_pcb_MV2c10_btag_ordered*(Iteration$<6))",
};
