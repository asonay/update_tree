#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "sm4t_total_mcbased_new/MyWeight.C"
#include "sm4t_total_mcbased_new/reweighting_functions_HT_all_red.C"

//#include "MyWeight.C"

using namespace std;

void update_ntup_1l(string loc)
//int main(int argc,char **argv)
{
  string filename = loc;
  //if (argc>1)
  //{filename = argv[1];}
  //else
  //{cout << "Please specify the file name." << endl; return 0;}

  
  TFile *f0 = new TFile(filename.c_str());
  TTree *tr = (TTree*)f0->Get("nominal_Loose");

  int n = tr->GetEntries();
  
  vector<float> w_ht_red,w_njets,w_dr;

  float HT_all,weight_normalise,weight_mc,weight_pileup,weight_leptonSF,weight_jvt,weight_bTagSF_MV2c10_Continuous,deltaR_avg_jets;
  int nJets,runNumber,nBTags_MV2c10_70,cls;
  unsigned chn;

  tr->SetBranchAddress("HT_all",&HT_all);
  tr->SetBranchAddress("nJets",&nJets);
  tr->SetBranchAddress("deltaR_avg_jets",&deltaR_avg_jets);
  tr->SetBranchAddress("mcChannelNumber",&chn);
  tr->SetBranchAddress("HF_SimpleClassification",&cls);
  
  for (int i=0;i<n;i++){
    tr->GetEntry(i);
    w_ht_red.push_back(MyWeight_HT_all_red(((HT_all/1000)-(nJets-7)*90), nJets, 1, 0, 0));
    w_njets.push_back(MyWeight_nJets(nJets,nJets,1));
    w_dr.push_back(MyWeight_deltaR_avg_jets(deltaR_avg_jets,nJets,1));
  }

  f0->Close();
  //----------------------------------------------------------------------------//

  cout << "Opening File: " << filename  << endl;
  
  
  TFile *f = new TFile(filename.c_str(),"update");


  TTree *T = (TTree*)f->Get("nominal_Loose");

  float jet_n_pt,jet_ns_pt,rw_ht_red,rw_njet,rw_dr,weight_gen,weight_factor,rcjet_d12_top,rcjet_d23_top,rcjet_pt_top;
  int event_number;
 
  TBranch *b_jet_n_pt = T->Branch("jet_n_pt",&jet_n_pt,"jet_n_pt/F");
  TBranch *b_jet_ns_pt = T->Branch("jet_ns_pt",&jet_n_pt,"jet_ns_pt/F");
  TBranch *b_weight_gen = T->Branch("weight_gen",&weight_gen,"weight_gen/F");
  TBranch *b_weight_factor = T->Branch("weight_factor",&weight_factor,"weight_factor/F");
  TBranch *b_rw_ht_red = T->Branch("rw_ht_red",&rw_ht_red,"rw_ht_red/F");
  TBranch *b_rw_njet = T->Branch("rw_njet",&rw_njet,"rw_njet/F");
  TBranch *b_rw_dr = T->Branch("rw_dr",&rw_dr,"rw_dr/F");
  TBranch *b_rcjet_d12_top = T->Branch("rcjet_d12_top",&rcjet_d12_top,"rcjet_d12_top/F");
  TBranch *b_rcjet_d23_top = T->Branch("rcjet_d23_top",&rcjet_d23_top,"rcjet_d23_top/F");
  TBranch *b_rcjet_pt_top = T->Branch("rcjet_pt_top",&rcjet_pt_top,"rcjet_pt_top/F");
  TBranch *b_event_number = T->Branch("event_number",&event_number,"event_number/I");

  vector<float> *jet_pt = 0;
  vector<float> *rcjet_d23 = 0;
  vector<float> *rcjet_d12 = 0;
  vector<float> *rcjet_pt = 0;

  T->SetBranchAddress("jet_pt",&jet_pt);
  T->SetBranchAddress("HT_all",&HT_all);
  T->SetBranchAddress("nJets",&nJets);
  T->SetBranchAddress("runNumber",&runNumber);
  T->SetBranchAddress("weight_normalise",&weight_normalise);
  T->SetBranchAddress("weight_mc",&weight_mc);
  T->SetBranchAddress("weight_pileup",&weight_pileup);
  T->SetBranchAddress("weight_leptonSF",&weight_leptonSF);
  T->SetBranchAddress("weight_jvt",&weight_jvt);
  T->SetBranchAddress("weight_bTagSF_MV2c10_Continuous",&weight_bTagSF_MV2c10_Continuous);
  T->SetBranchAddress("deltaR_avg_jets",&deltaR_avg_jets);
  T->SetBranchAddress("rcjet_d12",&rcjet_d12);
  T->SetBranchAddress("rcjet_d23",&rcjet_d23);
  T->SetBranchAddress("rcjet_pt",&rcjet_pt);


  double factor,factor2,sum;
  int nentries = T->GetEntries();

  for (int i=0;i<nentries;i++){

    T->GetEntry(i);

    event_number=i;

    if (jet_pt->size()==5)
      jet_n_pt = jet_pt->at(4);
    else if (jet_pt->size()==6)
      jet_n_pt = jet_pt->at(5);
    else if (jet_pt->size()==7)
      jet_n_pt = jet_pt->at(6);
    else if (jet_pt->size()>=8)
      jet_n_pt = jet_pt->at(jet_pt->size()-1);

    jet_ns_pt=0;
    for (int i=6;i<jet_pt->size();i++)
      jet_ns_pt += jet_pt->at(i);

    if (nJets>=7 && nBTags_MV2c10_70 == 2)
      factor = 1.0;
    else if (nJets==7 && nBTags_MV2c10_70 == 3)
      factor = 1.14;
    else if (nJets==8 && nBTags_MV2c10_70 == 3)
      factor = 1.15;
    else if (nJets==9 && nBTags_MV2c10_70 == 3)
      factor = 1.14;
    else if (nJets>=10 && nBTags_MV2c10_70 == 3)
      factor = 1.12;
    else if (nJets==7 && nBTags_MV2c10_70 >= 4)
      factor = 1.23;
    else if (nJets==8 && nBTags_MV2c10_70 >= 4)
      factor = 1.19;
    else if (nJets==9 && nBTags_MV2c10_70 >= 4)
      factor = 1.17;
    else if (nJets>=10 && nBTags_MV2c10_70 >= 4)
      factor = 1.25;
    else
      factor = 1.0;

    weight_factor=factor;
    
    rw_ht_red = w_ht_red[i];
    rw_njet = w_njets[i];
    rw_dr = w_dr[i];

    if (runNumber == 284500)
      factor2 = 36184.86;
    else if (runNumber == 300000)
      factor2 = 43587.3;
    else if (runNumber == 310000)
      factor2 = 45691.0;
    else
      factor2 = 1.0;
    
    
    weight_gen = (weight_normalise*weight_mc*weight_pileup*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_Continuous)*factor2;

    sum = 0.0;
    for (int i=0;i<rcjet_d12->size();i++)
      sum += pow(rcjet_d12->at(i),2);
    rcjet_d12_top = sqrt(sum);

    sum = 0.0;
    for (int i=0;i<rcjet_d23->size();i++)
      sum += pow(rcjet_d23->at(i),2);
    rcjet_d23_top = sqrt(sum);

    sum = 0.0;
    for (int i=0;i<rcjet_pt->size();i++)
      sum += rcjet_pt->at(i);
    rcjet_pt_top = sum;

    //cout << "ID: " << i << "  RW:  " << rw_ht_red << "  WG:  " << weight_gen << endl;

    b_jet_n_pt->Fill();
    b_jet_ns_pt->Fill();
    b_weight_gen->Fill();
    b_weight_factor->Fill();
    b_rw_ht_red->Fill();
    b_rw_njet->Fill();
    b_rw_dr->Fill();
    b_rcjet_d12_top->Fill();
    b_rcjet_d23_top->Fill();
    b_rcjet_pt_top->Fill();
    b_event_number->Fill();
    
  }

  
  T->Print();
  T->Write();

  delete f;

}
