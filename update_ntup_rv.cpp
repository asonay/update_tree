#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "MyWeight.C"


using namespace std;

void update_ntup_rv(string loc,bool bg = false)
//int main(int argc,char **argv)
{
  string filename = loc;

  TFile *f = new TFile((filename).c_str(),"update");
  TTree *T = (TTree*)f->Get("nominal_Loose");

  double rand;
  TBranch *b_rand = T->Branch("rand2",&rand,"rand2/D");
  
  TF1 *func = new TF1("func","exp(-x*1.5)",0,1);

  for (int i=0;i<T->GetEntries();i++){

    rand = func->GetRandom(0.0,1.0);

    if (bg) rand = 1.0-rand;

    cout << rand << endl;
    
    b_rand->Fill();

  }

  T->Print();
  T->Write("nominal_Loose");

  delete f;

}
